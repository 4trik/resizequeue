// Unfortunately, for unknown reasons, autociv mod overwrites
// the init() function, instead of making a proxy of it. This
// is very annoying compatibility-wise, besides being bad
// practice.
//
// For this reason we are forced to include autociv-dependent
// code to make this mod compatibile with autociv.

if (!global.g_autociv_optionsFiles)
    var g_autociv_optionsFiles = ["gui/options/options.json"];
g_autociv_optionsFiles.push("gui/options/options~ResizeQueue.json");

// PROXY init()

// Wrapper for the "init" function
function init_Wrapper_ResizeQueue(target, args) {
    // Run original function
    target(...args);

    // Run additional code
    const options = Engine.ReadJSONFile("gui/options/options~ResizeQueue.json");
    g_Options = g_Options.concat(options);

    // TODO: translation. The expected command (below) raises an error
    // translateObjectKeys(g_Options, ["label", "tooltip"]);

    placeTabButtons(
        g_Options,
        false,
        g_TabButtonHeight,
        g_TabButtonDist,
        selectPanel,
        displayOptions
    );

    // Write default values if missing
    const prefix = "ResizeQueue.";
    let settings = {};
    Object.keys(settings).filter(key => !Engine.ConfigDB_GetValue("user", key)).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
    Engine.ConfigDB_WriteFile("user", "config/user.cfg");

};

// Handler for the "init" function
const init_Handler_ResizeQueue = {
    apply: function (target, thisArg, args) {
        return init_Wrapper_ResizeQueue(target, args);
    }
};

// Proxy the "init" function
init = new Proxy(init, init_Handler_ResizeQueue);
