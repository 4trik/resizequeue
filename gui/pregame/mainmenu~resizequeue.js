var resizequeueConfig = {
	"needsToSave": false,
	"needsToReloadHotkeys": false,
	set(key, value) {
		Engine.ConfigDB_CreateValue("user", key, value);
		this.needsToSave = true;
		this.needsToReloadHotkeys = this.needsToReloadHotkeys || key.startsWith("hotkey.");
	},
	get(key) { return Engine.ConfigDB_GetValue("user", key); },
	save() {
		if (this.needsToSave) Engine.ConfigDB_WriteFile("user", "config/user.cfg");
		if (this.needsToReloadHotkeys) Engine.ReloadHotkeys();
	}
};

function resizeQueue_initCheck() {

	// Check settings
	{
		const settings = Engine.ReadJSONFile("resizeQueue_data/default_config.json");

		const allHotkeys = new Set(Object.keys(Engine.GetHotkeyMap()));
		for (const key in settings) {
			if (key.startsWith("hotkey.")) {
				if (!allHotkeys.has(key.slice("hotkey.".length))) {
					resizequeueConfig.set(key, settings[key]);
				}
			}
			else if (resizequeueConfig.get(key) == "")
				resizequeueConfig.set(key, settings[key]);

			else if ((key == "xres" || key == "yres") && resizequeueConfig.get(key) == "0")
				resizequeueConfig.set(key, settings[key]);
		}
		resizequeueConfig.save();

	}

	resizequeueConfig.save()
}
let state = resizeQueue_initCheck();
