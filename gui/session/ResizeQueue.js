class ResizeQueue {

    constructor() {
        this.modes = new Array;
        this.allyToTribute = new Array;
        this.playerEntities = this.getPlayerEntities();
        this.hasBeenUpdated = false;
        this.timeOfLastOrder = 3000;
        this.timeOfLastBarter = 3000;
        this.lastTrainerOrderedID = 0;
        this.hasMarket = false;
        this.setAutoQueue = new Array;
        this.resourceType = [
            "food",
            "wood",
            "stone",
            "metal"
        ];
        this.resource = {
            "counts": {},
            "gatherers": {},
            "rates": {}
        };
        this.userRefRes = {
            "food": 0,
            "wood": 0,
            "stone": 0,
            "metal": 0
        };
    }
    getUnspendableResources() {
        return {

            "food": Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.foodReserve")),
            "wood": Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.woodReserve")),
            "stone": Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.stoneReserve")),
            "metal": Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.metalReserve"))
        }
    }
    getPlayersStates() {
        const playersStates = Engine.GuiInterfaceCall("GetSimulationState", {
            g_IsObserver, g_ViewedPlayer, g_LastTickTime
        }).players ?? [];
        return playersStates[g_ViewedPlayer];
    }
    tickTasks() {
        if (!g_IsObserver) {
            this.playerEntities = this.getPlayerEntities();
            this.userStates = this.getPlayersStates();
            this.idleTrainers = this.getNumberOfIdleTrainers();
            this.trainCitizens();
            this.checkIfUpdated();
        }
    }
    checkIfUpdated() {
        let state = GetEntityState(this.lastTrainerOrdered);
        if (g_SimState.timeElapsed > this.timeOfLastOrder) {
            if (!GetEntityState(this.lastTrainerOrdered))
                this.hasBeenUpdated = true;
            else if ((state.production.queue.length != 0 && state.production.queue[0].timeRemaining > 1000) || g_SimState.timeElapsed - this.timeOfLastOrder > 1200)
                this.hasBeenUpdated = true;
        }
    }
    getNumberOfIdleTrainers() {
        let num = 0;
        try {
            // Loop through all the Trainer entities owned by the player
            for (let trainerID in this.playerEntities.Trainer) {
                // Get the state of each Trainer entity
                let state = GetEntityState(this.playerEntities.Trainer[trainerID]);

                // Check if the production queue of the Trainer entity is empty or if there is only one item in the queue and its progress is greater than 0.9
                if ((state?.production?.queue.length === 0  || (state?.production?.queue.length === 1 && (state?.production?.queue[0].progress > 0.9) || state?.production?.queue[0].timeRemaining <= 1200)) && state.production.autoqueue) {
                    num++;
                }
            }
        } catch (error) {
            // Catch any errors that may occur during execution
            warn('An error occurred while retrieving the number of idle Trainers:', error);
            return -1; // Return -1 to indicate an error occurred
        }
        // Return the number of idle Trainers
        return num;
    }
    cacheStats(unit, batchSize, state) {
        let unitCost = GetTemplateData(unit).cost;
        if (hasClass(state, "Corral"))
            this.userStates.classCounts.Animal += batchSize;
        else
            this.userStates.popCount += batchSize * unitCost.population;
        for (const resType of ["food", "wood", "stone", "metal"]) {
            if (unitCost[resType] > 0)
                this.userStates.resourceCounts[resType] -= unitCost[resType] * batchSize;
        }
        this.idleTrainers--;
    }
    splitTrainableUnitsAndCheck(template, trainableUnits) {
        if (template) {
            let unitCost = GetTemplateData(template).cost;
            const maxTrainingBatchSize = Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.MaxBatchSize"));
            if (trainableUnits > 1 && this.idleTrainers > 1)
                trainableUnits = Math.max(Math.floor(trainableUnits / this.idleTrainers), 1);
            if (trainableUnits > Math.ceil(maxTrainingBatchSize / (unitCost.population)))
                trainableUnits = Math.ceil(maxTrainingBatchSize / (unitCost.population));
            if (trainableUnits * unitCost.population > this.userStates.popLimit - this.userStates.popCount && trainableUnits >= 1)
                trainableUnits = Math.floor((this.userStates.popLimit - this.userStates.popCount) / unitCost.population);
            return trainableUnits;
        }
        return 0;
    }
    getTrainableUnits(template) {
        let unitCost = GetTemplateData(template).cost;
        let trainableUnits = Math.floor(this.userStates.popLimit - this.userStates.popCount / unitCost.population);
        const maxTrainingBatchSize = Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.MaxBatchSize"));
        let i = 0;
        for (const resType of this.resourceType) {
            let unspendableResources = this.getUnspendableResources()[resType];
            let spendableRes = this.userStates.resourceCounts[resType] - unspendableResources;
            if (inputState != 0 && placementSupport?.template)
                spendableRes = this.userStates.resourceCounts[resType] - Math.max(unspendableResources, GetTemplateData(placementSupport.template).cost[resType]);
            if (spendableRes < 0)
                spendableRes = 0;
            if (trainableUnits > spendableRes / unitCost[resType]) {
                trainableUnits = Math.floor(spendableRes / unitCost[resType]);
            }
            i++;
        }
        if (trainableUnits > Math.floor(maxTrainingBatchSize / (unitCost.population)))
            trainableUnits = Math.floor(maxTrainingBatchSize / (unitCost.population));
        return trainableUnits;
    }
    getTrainableUnitsCorrals(template) {
        let unitCost = GetTemplateData(template).cost;
        let trainableUnits = Math.round(Engine.ConfigDB_GetValue("user", "boongui.trainer.corralBatchSize"));
        let i = 0;
        for (const resType of this.resourceType) {
            let unspendatbleResources = 0;
            let spendableRes = this.userStates.resourceCounts[resType] - unspendatbleResources;
            if (spendableRes < 0)
                spendableRes = 0;
            if (trainableUnits > spendableRes / unitCost[resType]) {
                trainableUnits = Math.floor(spendableRes / unitCost[resType]);
            }
            i++;
        }
        if (this.userStates.classCounts.Animal + trainableUnits > 50) //HARDCODED animal limit !
            trainableUnits = this.userStates.classCounts.Animal - 50;
        return trainableUnits;
    }
    trainCitizens() {
        if (this.hasBeenUpdated == true) {

            for (let trainerID in this.playerEntities.Trainer) {

                let state = GetEntityState(this.playerEntities.Trainer[trainerID]);
                let unitToTrain = false;
                let unitToTrainQty = 0;
                if (state?.production?.queue[0]?.unitTemplate != undefined || state?.production?.queue.length === 0) {


                    if (this.setAutoQueue.findIndex(x => x.id == trainerID) != -1) {

                        unitToTrain = this.setAutoQueue.find(x => x.id == trainerID).unit;
                        unitToTrainQty = this.getTrainableUnits(unitToTrain);
                        if (!state.production.autoqueue && this.setAutoQueue.find(x => x.id == trainerID)?.queue == true) {
                            Engine.PostNetworkCommand({
                                "type": "autoqueue-on",
                                "entities": JSON.parse("[" + JSON.stringify(this.playerEntities.Trainer[trainerID]) + "]")
                            });
                            this.setAutoQueue.find(x => x.id == trainerID).queue = false;
                        }
                        if (unitToTrainQty >= 1)
                            this.setAutoQueue = this.setAutoQueue.filter(x => x.id != trainerID);

                    }
                    else if (state.production?.autoqueue && state.production?.queue[0]) {
                        unitToTrain = state.production.queue[0].unitTemplate;
                        unitToTrainQty = this.getTrainableUnits(unitToTrain);
                    }
                    if (unitToTrain != false) {

                        const template = unitToTrain;
                        let trainableUnits = unitToTrainQty;

                        if ((state.production.queue.length === 0 || ((state.production.queue.length === 1 || state.production.autoqueue) && state.production.queue[0].timeRemaining <= 1000))) {
                            trainableUnits = this.splitTrainableUnitsAndCheck(unitToTrain, unitToTrainQty);
                            if (state.production.autoqueue && state.production.queue.length != 0) {
                                Engine.PostNetworkCommand({
                                    "type": "autoqueue-off",
                                    "entities": JSON.parse("[" + JSON.stringify(this.playerEntities.Trainer[trainerID]) + "]")
                                });
                                if (this.setAutoQueue.findIndex(x => x.id == trainerID && x.unit == unitToTrain) === -1) {
                                    this.setAutoQueue = this.setAutoQueue.filter(x => x.id != trainerID);
                                    this.setAutoQueue.push({ "id": trainerID, "unit": state.production.queue[0].unitTemplate, "queue": true });
                                }
                                else if (this.setAutoQueue.findIndex(x => x.id == trainerID && x.unit == unitToTrain) != -1 && this.setAutoQueue.find(x => x.id == trainerID && x.unit == unitToTrain).queue == false) {
                                    this.setAutoQueue.find(x => x.id == trainerID && x.unit == unitToTrain).queue = true;
                                }
                            }
                            if (hasClass(state, "Corral")) {
                                trainableUnits = this.getTrainableUnitsCorrals(template);
                            }
                            if (trainableUnits >= 1) {
                                const batchMultiple = Math.round(Engine.ConfigDB_GetValue("user", "boongui.trainer.batchMultiple"));
                                trainableUnits = (trainableUnits < batchMultiple) ? trainableUnits : trainableUnits - (Math.floor(trainableUnits % batchMultiple));
                                this.hasBeenUpdated = false;
                                this.lastTrainerOrdered = this.playerEntities.Trainer[trainerID];
                                this.timeOfLastOrder = g_SimState.timeElapsed;
                                Engine.PostNetworkCommand({
                                    "type": "train",
                                    "entities": JSON.parse("[" + JSON.stringify(this.playerEntities.Trainer[trainerID]) + "]"),
                                    "template": template,
                                    "count": trainableUnits,
                                    "pushFront": false
                                });
                                this.cacheStats(template, trainableUnits, state);
                            }
                        }
                    }
                }
            }
        }
    }

    // Return an object of player entities
    getPlayerEntities() {
        this.unitsToIgnore = 0;
        let playerEntities = {
            Trainer: [],
        };
        for (let mode in this.modes) {
            this.modes[mode].count = 0;
        }
        const interfacePlayerEntities = Engine.GuiInterfaceCall("GetPlayerEntities");
        for (let entityId of interfacePlayerEntities) {
            let state = GetEntityState(entityId);
            let templatedata = GetTemplateData(state.template);

            for (let key in playerEntities) {

                if (hasClass(state, "Foundation"))
                    break;

                const classesList = GetTemplateData(state.template).visibleIdentityClasses;
                let configClasses = Engine.ConfigDB_GetValue("user", "boongui.trainer.trainerClasses").split(",");
                if(configClasses.length < 2)
                    configClasses = ["CivilCentre", "Military", "Dock", "Temple", "House", "Market", "Palace"]
                if (classesList)
                if (configClasses.some(c => classesList.includes(c))
                    && Engine.ConfigDB_GetValue("user", "boongui.trainer.enableUnits") == "true" || (hasClass(state, "Corral") && Engine.ConfigDB_GetValue("user", "boongui.trainer.enableCorral") == "true")) {
                    playerEntities["Trainer"].push(entityId);
                    break;
                }

            }
        }

        return playerEntities;
    }

}

