var g_ResizeQueue;

// PROXY init()

// Wrapper for the "init" function
function init_Wrapper_ResizeQueue(target, args)
{
    // Run original function
    target(...args);

    // Run additional code
    g_ResizeQueue = new ResizeQueue();
}

// Handler for the "init" function
const init_Handler_ResizeQueue = {
    apply: function(target, thisArg, args) {
        return init_Wrapper_ResizeQueue(target, args);
    }
};

// Proxy the "init" function
init = new Proxy(init, init_Handler_ResizeQueue);

// PROXY onTick()

// Wrapper for the "onTick" function
function onTick_Wrapper_ResizeQueue(target, args)
{
    // Run original function
    target(...args);

    // Run additional code
    if (!g_ResizeQueue)
        return;
    else
        g_ResizeQueue.tickTasks();
}

// Handler for the "onTick" function
const onTick_Handler_ResizeQueue = {
    apply: function(target, thisArg, args) {
        return onTick_Wrapper_ResizeQueue(target, args);
    }
};

// Proxy the "onTick" function
onTick = new Proxy(onTick, onTick_Handler_ResizeQueue);
